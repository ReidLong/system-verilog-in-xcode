//
//  AppDelegate.h
//  SyntaxHighlightAutoDetect
//
//  Created by Reid Long on 10/13/15.
//  Copyright © 2015 Reid Long. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

