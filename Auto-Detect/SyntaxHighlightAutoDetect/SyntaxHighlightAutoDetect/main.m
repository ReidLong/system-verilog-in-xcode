//
//  main.m
//  SyntaxHighlightAutoDetect
//
//  Created by Reid Long on 10/13/15.
//  Copyright © 2015 Reid Long. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
