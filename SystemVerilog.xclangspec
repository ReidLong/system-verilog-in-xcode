(
	{
		Identifier = "xcode.lang.systemverilog.literals";
		Syntax = {
			Chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'";
			StartChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'";
			Match = (
				"^[0-9]+'[bBhHdD][0-9A-Za-z]+",
				"^[0-9]+",
			);
			Type = "xcode.syntax.number";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.block.case";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Start = "case|casex";
			End = endcase;
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.systemverilog.block",
				"xcode.lang.systemverilog.block.case",
				"xcode.lang.systemverilog.bracketexpr",
				"xcode.lang.systemverilog.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.module.close";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"endmodule:",
				"xcode.lang.systemverilog.identifier",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.module.name";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"xcode.lang.systemverilog.identifier",
				"xcode.lang.systemverilog.parenexpr?",
				";",
			);
			Type = "xcode.syntax.name.partial";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.module.declarator";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				module,
				"xcode.lang.systemverilog.module.name",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.module";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Start = "xcode.lang.systemverilog.module.declarator";
			End = endmodule;
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.systemverilog.block",
				"xcode.lang.systemverilog.block.case",
				"xcode.lang.systemverilog.bracketexpr",
				"xcode.lang.systemverilog.parenexpr",
			);
			Type = "xcode.syntax.definition.module";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.preprocessor";
		Syntax = {
			Start = "`";
			EscapeChar = "\\";
			End = "\n";
			StartAtBOL = YES;
			Tokenizer = "xcode.lang.systemverilog.preprocessor.lexer";
			Type = "xcode.syntax.preprocessor";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.preprocessor.lexer";
		Syntax = {
			IncludeRules = (
				"xcode.lang.string",
				"xcode.lang.character",
				"xcode.lang.number",
				"xcode.lang.systemverilog.preprocessor.identifier",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.preprocessor.identifier";
		Syntax = {
			StartChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
			Chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
			Words = (
				"default_nettype",
				none,
			);
			Type = "xcode.syntax.preprocessor.keyword";
			AltType = "xcode.syntax.preprocessor.identifier";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.identifier";
		Syntax = {
			StartChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_$";
			Chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
			Words = (
				always,
				and,
				assign,
				begin,
				buf,
				bufif0,
				bufif1,
				case,
				casex,
				casez,
				cmos,
				deassign,
				default,
				defparam,
				disable,
				edge,
				else,
				end,
				endcase,
				endmodule,
				endfunction,
				endprimitive,
				endspecify,
				endtable,
				endtask,
				event,
				for,
				force,
				forever,
				fork,
				function,
				highz0,
				highz1,
				if,
				ifnone,
				initial,
				inout,
				input,
				integer,
				join,
				large,
				logic,
				macromodule,
				medium,
				module,
				nand,
				negedge,
				nmos,
				nor,
				not,
				notif0,
				notif1,
				or,
				output,
				parameter,
				pmos,
				posedge,
				primitive,
				pull0,
				pull1,
				pullup,
				pulldown,
				rcmos,
				real,
				realtime,
				reg,
				release,
				repeat,
				rnmos,
				rpmos,
				rtran,
				rtranif0,
				rtranif1,
				scalared,
				small,
				specify,
				specparam,
				strong0,
				strong1,
				supply0,
				supply1,
				table,
				task,
				time,
				tran,
				tranif0,
				tranif1,
				tri,
				tri0,
				tri1,
				triand,
				trior,
				trieg,
				vectored,
				wait,
				wand,
				weak0,
				weak1,
				while,
				wire,
				wor,
				xnor,
				xor,
			);
			Type = "xcode.syntax.keyword";
			AltType = "xcode.syntax.identifier";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.delay";
		Syntax = {
			StartChars = "#";
			Chars = 0123456789;
			Type = "xcode.syntax.identifier.constant";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog";
		Description = "SystemVerilog Coloring";
		IncludeInMenu = YES;
		BasedOn = "xcode.lang.simpleColoring";
		UsesCLikeIndentation = YES;
		Name = "System Verilog";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer.toplevel";
			IncludeRules = (
				"xcode.lang.systemverilog.function",
				"xcode.lang.systemverilog.function.knr",
				"xcode.lang.systemverilog.function.declaration",
				"xcode.lang.systemverilog.module",
				"xcode.lang.systemverilog.block.toplevel",
				"xcode.lang.systemverilog.bracketexpr",
				"xcode.lang.systemverilog.parenexpr",
			);
			Type = "xcode.syntax.plain";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.block.toplevel";
		BasedOn = "xcode.lang.systemverilog.block";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Start = begin;
			End = end;
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.systemverilog.function.declaration",
				"xcode.lang.systemverilog.function",
				"xcode.lang.systemverilog.typedef",
				"xcode.lang.systemverilog.block",
				"xcode.lang.systemverilog.bracketexpr",
				"xcode.lang.systemverilog.parenexpr",
				"xcode.lang.systemverilog.initializer",
			);
			Type = "xcode.syntax.plain";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.lexer.toplevel";
		Syntax = {
			IncludeRules = (
				"xcode.lang.systemverilog.literals",
				"xcode.lang.comment",
				"xcode.lang.comment.singleline",
				"xcode.lang.systemverilog.module.declarator",
				"xcode.lang.systemverilog.module.close",
				"xcode.lang.systemverilog.identifier",
				"xcode.lang.number",
				"xcode.lang.systemverilog.preprocessor",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.lexer";
		Syntax = {
			IncludeRules = (
				"xcode.lang.comment",
				"xcode.lang.string",
				"xcode.lang.systemverilog.literals",
				"xcode.lang.comment.singleline",
				"xcode.lang.systemverilog.preprocessor",
				"xcode.lang.completionplaceholder",
				"xcode.lang.systemverilog.attribute",
				"xcode.lang.systemverilog.identifier",
				"xcode.lang.number",
				"xcode.lang.systemverilog.delay",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.attribute";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer.attribute";
			Rules = (
				"__attribute__",
				"xcode.lang.systemverilog.parenexpr.attribute",
			);
			Type = "xcode.syntax.systemverilog.attribute";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.lexer.attribute";
		Syntax = {
			IncludeRules = (
				"xcode.lang.comment",
				"xcode.lang.comment.singleline",
				"xcode.lang.systemverilog.preprocessor",
				"xcode.lang.string",
				"xcode.lang.character",
				"xcode.lang.completionplaceholder",
				"xcode.lang.systemverilog.identifier",
				"xcode.lang.number",
				"xcode.lang.systemverilog.delay",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.parenexpr.attribute";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer.attribute";
			Start = "(";
			End = ")";
			Recursive = YES;
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.function.declaration";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"xcode.lang.systemverilog.function.declarator",
				"xcode.lang.systemverilog.identifier?",
				";",
			);
			Type = "xcode.syntax.declaration.c.function";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.function";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"xcode.lang.systemverilog.function.declarator",
				"xcode.lang.systemverilog.block",
			);
			Type = "xcode.syntax.definition.systemverilog.function";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.function.knr";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"xcode.lang.systemverilog.function.declarator",
				"xcode.lang.systemverilog.identifier|auto|char|const|double|enum|float|int|long|register|short|signed|struct|union|unsigned|void",
				"xcode.lang.systemverilog.knrargs+",
				"xcode.lang.systemverilog.block",
			);
			Type = "xcode.syntax.definition.systemverilog.function";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.knrargs";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"xcode.lang.systemverilog.identifier|auto|char|const|double|enum|float|int|long|register|short|signed|struct|union|unsigned|void|*|,*",
				"xcode.lang.systemverilog.parenexpr*",
				"xcode.lang.systemverilog.bracketexpr*",
				";",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.function.declarator";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"xcode.lang.systemverilog.function.name",
				"xcode.lang.systemverilog.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.function.name";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"xcode.lang.systemverilog.identifier",
			);
			Type = "xcode.syntax.name.partial";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.lexer.typedef.name";
		Syntax = {
			IncludeRules = (
				"xcode.lang.comment",
				"xcode.lang.comment.singleline",
				"xcode.lang.systemverilog.preprocessor",
				"xcode.lang.string",
				"xcode.lang.character",
				"xcode.lang.completionplaceholder",
				"xcode.lang.systemverilog.attribute",
				"xcode.lang.systemverilog.typedef.name",
				"xcode.lang.systemverilog.identifier",
				"xcode.lang.number",
				"xcode.lang.systemverilog.delay",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.typedef";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer.typedef";
			Start = typedef;
			End = "xcode.lang.systemverilog.typedef.end";
			AltEnd = ";";
			IncludeRules = (
				"xcode.lang.systemverilog.enumblock",
				"xcode.lang.systemverilog.structblock",
				"xcode.lang.systemverilog.unionblock",
				"xcode.lang.systemverilog.typedef.function",
				"xcode.lang.systemverilog.block",
				"xcode.lang.systemverilog.bracketexpr",
				"xcode.lang.systemverilog.parenexpr",
			);
			Type = "xcode.syntax.typedef";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.typedef.name";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"xcode.lang.systemverilog.identifier",
			);
			Type = "xcode.syntax.name.partial";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.typedef.end";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer.typedef.name";
			Rules = (
				"xcode.lang.systemverilog.typedef.name",
				";?",
			);
			Type = "xcode.syntax.name.tree";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.typedef.function";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer.typedef.name";
			Rules = (
				"(",
				"*",
				"xcode.lang.systemverilog.typedef.name",
				")",
			);
			Type = "xcode.syntax.name.tree";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.block";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Start = begin;
			End = end;
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.systemverilog.bracketexpr",
				"xcode.lang.systemverilog.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.typeblock";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Start = "{";
			End = "}";
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.systemverilog.bracketexpr",
				"xcode.lang.systemverilog.parenexpr",
				"xcode.lang.systemverilog.structblock",
				"xcode.lang.systemverilog.unionblock",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.enumblock";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"xcode.lang.systemverilog.enum.declarator|xcode.lang.systemverilog.nsenum.declarator",
				"xcode.lang.systemverilog.block",
			);
			Type = "xcode.syntax.declaration.enum";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.enum.declarator";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				enum,
				"xcode.lang.systemverilog.identifier?",
				":?",
				"xcode.lang.systemverilog.typeidentifier*",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.nsenum.declarator";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"NS_ENUM",
				"xcode.lang.systemverilog.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.structblock";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				struct,
				"xcode.lang.systemverilog.identifier?",
				":?",
				"xcode.lang.systemverilog.typeidentifier*",
				"xcode.lang.systemverilog.typeblock",
			);
			Type = "xcode.syntax.declaration.struct";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.unionblock";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				union,
				"xcode.lang.systemverilog.identifier?",
				":?",
				"xcode.lang.systemverilog.typeidentifier*",
				"xcode.lang.systemverilog.typeblock",
			);
			Type = "xcode.syntax.declaration.union";
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.typeidentifier";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Rules = (
				"xcode.lang.systemverilog.identifier|int|char|float|double|short|long|unsigned|signed",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.parenexpr";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Start = "(";
			End = ")";
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.systemverilog.block",
				"xcode.lang.systemverilog.bracketexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.bracketexpr";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Start = "[";
			End = "]";
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.systemverilog.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.systemverilog.initializer";
		Syntax = {
			Tokenizer = "xcode.lang.systemverilog.lexer";
			Start = "=";
			End = ";";
			Recursive = NO;
			IncludeRules = (
				"xcode.lang.systemverilog.block",
				"xcode.lang.systemverilog.parenexpr",
				"xcode.lang.systemverilog.bracketexpr",
			);
		};
	},
)