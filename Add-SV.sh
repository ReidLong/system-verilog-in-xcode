#!/bin/bash

PLISTBUDDY=/usr/libexec/PlistBuddy

DVTFOUNDATION_PATH="/Applications/Xcode.app/Contents/SharedFrameworks/DVTFoundation.framework/Versions/A/Resources/"

LANGUAGE_CONFIG_PATH="SV-Language.plist"

LANGUAGE_SPEC_PATH="SystemVerilog.xclangspec"

# Add Language to DVTFoundation data
$PLISTBUDDY "$DVTFOUNDATION_PATH/DVTFoundation.xcplugindata" -c 'Merge '"$LANGUAGE_CONFIG_PATH"' plug-in:extensions'


# Move the language specification to DVTFoundation Directory
cp "$LANGUAGE_SPEC_PATH" "$DVTFOUNDATION_PATH"


# Remove Plugin Cache
rm -f /private/var/folders/*/*/*/com.apple.DeveloperTools/*/Xcode/PlugInCache*.xcplugincache

