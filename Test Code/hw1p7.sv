`default_nettype none


int method() {
    return 0;
}

// Green
module howManyOnes
    (input logic [2:0] N,
    output logic M1, M2, M3);
    
    or (M1, N[0], N[1], N[2]);
    and (M3, N[0], N[1], N[2]);
    
    logic f1, f2, f3;
    or (f1, N[2], N[1]);
    and (f2, N[2], N[1]),
    (f3, f1, N[0]);
    
    or (M2, f3, f2);

endmodule howManyOnes

// Green
module IsBiggerThan
    (input logic [5:0] N,
    output logic M3, M5);
 
 logic top1, top2, top3, bot1, bot2, bot3;
 howManyOnes (N[5], N[4], N[3], top1, top2, top3),
 (N[2], N[1], N[0], bot1, bot2, bot3);
 
 logic anyTop, anyBot;
 or (anyTop, top1, top2),
 (anybot, bot1, bot2);
 
 logic topSet, botSet;
 and (topSet, top2, anyBot),
 (botSet, bot2, anyTop);
 
 or (M3, top3, bot3, topSet, botSet);

endmodule: IsBiggerThan

module IsBiggerThan_test
    (output logic [5:0] N,
   input logic M3, M5);

logic [5:0] test;
logic computedM3, computedM5;
logic failed;

   initial begin
   failed = 0;
   for(test = 0; test < 64; test++) begin
   integer count = test[0] + test[1] + test[2] + test[3] + test[4] + test[5];
   N = test;
   #10;
   computedM3 = count >= 3;
   computedM5 = count >= 5;
   if(M5 && ~M5) begin
   $display("Mismatch! Actual: M5=%b ComputedM5=%b", M5, computedM5);
   failed = 1;
   end

    if(M3 && ~M3) begin
    $display("Mismatch! Actual: M3=%b ComputedM3=%b", M3, computedM3);
    failed = 1;
    end
    end
if (~failed) $display("Circuit test succeeded!");
        end // initial begin
endmodule: IsBiggerThan_test
      
module system;
logic [5:0] N;
logic M3, M5;

   IsBiggerThan  mod (N, M3, M5);
   IsBiggerThan_test  test (N, M3, M5);

   endmodule: system
