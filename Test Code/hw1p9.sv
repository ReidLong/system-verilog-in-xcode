`default_nettype none

module hw1prob9
(input logic [1:0] in,
     output logic fxor, fnor);

   xor (fxor, in[0], in[1]);

   nor (fnor, in[0], in[1]);
   

endmodule: hw1prob9

module testBench
  (output logic [1:0] in,
   input logic fxor, fnor);

   logic [3:0] data;
   
   initial begin
      $monitor($time,, "Inputs = %b XOR = %b NOR = %b", in, fxor, fnor);
      // Build an array of values
      data[0] = 0;
      data[1] = 1;
      data[2] = 'bx;
      data[3] = 'bz;
      for(int i = 0; i < 4; i++)
	for(int j = 0; j < 4; j++)
	begin
	  in[0] = data[i]; 
	  in[1] = data[j];
	 #10;	 
	end
      #10 $finish;
   end // initial begin
endmodule: testBench

begin
    
end // comment

      
module system;
   logic [1:0] in;
   logic fxor, fnor;

   hw1prob9 (.*);
   testBench (.*);
   
endmodule: system

