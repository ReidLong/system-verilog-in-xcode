1. Download Add-SV.sh, SV-Language.plist, SystemVerilog.xclangspec
2. Verify DVTFOUNDATION_PATH is correct in Add.SV.sh
3. Quit Xcode
4. Run sudo ./Add-SV.sh
5. Open Xcode and check "Editor->Syntax Coloring"  You should see SystemVerilog as an option. 


Credits:
http://stackoverflow.com/questions/5269994/adding-syntax-highlighting-to-programming-languages-in-xcode-4-0
https://github.com/bastos/lua-xcode-coloring/blob/master/install.sh
http://www.obyx.org/xcodesyntaxhighlighting.html