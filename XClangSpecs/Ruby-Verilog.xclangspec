(
	{
		Identifier = "xcode.lang.ruby.identifier";
		Syntax = {
			StartChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_[";
			Chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_?[]";
			Words = (
				always,
				and,
				assign,
				begin,
				buf,
				bufif0,
				bufif1,
				case,
				casex,
				casez,
				cmos,
				deassign,
				default,
				defparam,
				disable,
				edge,
				else,
				end,
				endcase,
				endmodule,
				endfunction,
				endprimitive,
				endspecify,
				endtable,
				endtask,
				event,
				for,
				force,
				forever,
				fork,
				function,
				highz0,
				highz1,
				if,
				ifnone,
				initial,
				inout,
				input,
				integer,
				join,
				large,
				macromodule,
				medium,
				module,
				nand,
				negedge,
				nmos,
				nor,
				not,
				notif0,
				notif1,
				or,
				output,
				parameter,
				pmos,
				posedge,
				primitive,
				pull0,
				pull1,
				pullup,
				pulldown,
				rcmos,
				real,
				realtime,
				reg,
				release,
				repeat,
				rnmos,
				rpmos,
				rtran,
				rtranif0,
				rtranif1,
				scalared,
				small,
				specify,
				specparam,
				strong0,
				strong1,
				supply0,
				supply1,
				table,
				task,
				time,
				tran,
				tranif0,
				tranif1,
				tri,
				tri0,
				tri1,
				triand,
				trior,
				trieg,
				vectored,
				wait,
				wand,
				weak0,
				weak1,
				while,
				wire,
				wor,
				xnor,
				xor,
			);
			Type = "xcode.syntax.keyword";
			AltType = "xcode.syntax.identifier";
		};
	},
	{
		Identifier = "xcode.lang.ruby";
		Description = "Ruby Coloring";
		BasedOn = "xcode.lang.simpleColoring";
		IncludeInMenu = YES;
		Name = Ruby;
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer.toplevel";
			IncludeRules = (
				"xcode.lang.ruby.module",
				"xcode.lang.ruby.class",
				"xcode.lang.ruby.function",
				"xcode.lang.ruby.block",
				"xcode.lang.ruby.block.if",
				"xcode.lang.ruby.block.while",
				"xcode.lang.ruby.block.until",
				"xcode.lang.ruby.block.unless",
				"xcode.lang.ruby.block.for",
				"xcode.lang.ruby.block.case",
				"xcode.lang.ruby.block.begin",
				"xcode.lang.ruby.block.do",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
			Type = "xcode.syntax.plain";
		};
	},
	{
		Identifier = "xcode.lang.ruby.inxml";
		BasedOn = "xcode.lang.ruby";
		Syntax = {
			Start = "<%";
			End = "%>";
			Foldable = YES;
			"ParseEndBeforeIncludedRules" = YES;
			IncludeRules = (
				"xcode.lang.ruby",
			);
			Type = "xcode.syntax.keyword";
		};
	},
	{
		Identifier = "xcode.lang.ruby.inxml2";
		BasedOn = "xcode.lang.ruby";
		Syntax = {
			Start = "<%%";
			End = "%>";
			Foldable = YES;
			"ParseEndBeforeIncludedRules" = YES;
			IncludeRules = (
				"xcode.lang.ruby",
			);
			Type = "xcode.syntax.keyword";
		};
	},
	{
		Identifier = "xcode.lang.ruby.lexer";
		Syntax = {
			IncludeRules = (
				"xcode.lang.ruby.comment",
				"xcode.lang.ruby.stringeval",
				"xcode.lang.comment.singleline.pound",
				"xcode.lang.string",
				"xcode.lang.character",
				"xcode.lang.ruby.identifier",
				"xcode.lang.number",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.lexer.toplevel";
		Syntax = {
			IncludeRules = (
				"xcode.lang.ruby.comment",
				"xcode.lang.ruby.stringeval",
				"xcode.lang.comment.singleline.pound",
				"xcode.lang.string",
				"xcode.lang.character",
				"xcode.lang.ruby.module.declarator",
				"xcode.lang.ruby.class.declarator",
				"xcode.lang.ruby.function.declarator",
				"xcode.lang.ruby.identifier",
				"xcode.lang.number",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.module";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer.toplevel";
			Start = "xcode.lang.ruby.module.declarator";
			End = end;
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.ruby.class",
				"xcode.lang.ruby.function",
			);
			Type = "xcode.syntax.definition.class";
		};
	},
	{
		Identifier = "xcode.lang.ruby.module.declarator";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Rules = (
				module,
				"xcode.lang.ruby.identifier",
			);
			Type = "xcode.syntax.name.partial";
		};
	},
	{
		Identifier = "xcode.lang.ruby.class";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer.toplevel";
			Start = "xcode.lang.ruby.class.declarator";
			End = end;
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.ruby.function",
			);
			Type = "xcode.syntax.definition.class";
		};
	},
	{
		Identifier = "xcode.lang.ruby.class.declarator";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Rules = (
				class,
				"xcode.lang.ruby.identifier",
				"xcode.lang.ruby.superclass?",
			);
			Type = "xcode.syntax.name.partial";
		};
	},
	{
		Identifier = "xcode.lang.ruby.superclass";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Rules = (
				"<",
				"xcode.lang.ruby.identifier",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.function";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer.toplevel";
			Start = "xcode.lang.ruby.function.declarator";
			End = end;
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.ruby.block",
				"xcode.lang.ruby.block.if",
				"xcode.lang.ruby.block.while",
				"xcode.lang.ruby.block.until",
				"xcode.lang.ruby.block.unless",
				"xcode.lang.ruby.block.for",
				"xcode.lang.ruby.block.case",
				"xcode.lang.ruby.block.begin",
				"xcode.lang.ruby.block.do",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
			Type = "xcode.syntax.definition.function";
		};
	},
	{
		Identifier = "xcode.lang.ruby.function.declarator";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Rules = (
				def,
				"xcode.lang.ruby.function.name",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.function.name";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Rules = (
				"xcode.lang.ruby.identifier",
				"xcode.lang.ruby.function.name.more?",
			);
			Type = "xcode.syntax.name.partial";
		};
	},
	{
		Identifier = "xcode.lang.ruby.function.name.more";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Rules = (
				".",
				"xcode.lang.ruby.identifier",
			);
			Type = "xcode.syntax.name.partial";
		};
	},
	{
		Identifier = "xcode.lang.ruby.block";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = "{";
			End = "}";
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.ruby.block.if",
				"xcode.lang.ruby.block.while",
				"xcode.lang.ruby.block.until",
				"xcode.lang.ruby.block.unless",
				"xcode.lang.ruby.block.for",
				"xcode.lang.ruby.block.case",
				"xcode.lang.ruby.block.begin",
				"xcode.lang.ruby.block.do",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.block.if";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = if;
			End = end;
			Foldable = YES;
			Recursive = YES;
			StartAtBOL = YES;
			IncludeRules = (
				"xcode.lang.ruby.block",
				"xcode.lang.ruby.block.while",
				"xcode.lang.ruby.block.until",
				"xcode.lang.ruby.block.unless",
				"xcode.lang.ruby.block.for",
				"xcode.lang.ruby.block.case",
				"xcode.lang.ruby.block.begin",
				"xcode.lang.ruby.block.do",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.block.while";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = while;
			End = end;
			Foldable = YES;
			Recursive = YES;
			StartAtBOL = YES;
			IncludeRules = (
				"xcode.lang.ruby.block",
				"xcode.lang.ruby.block.if",
				"xcode.lang.ruby.block.until",
				"xcode.lang.ruby.block.unless",
				"xcode.lang.ruby.block.for",
				"xcode.lang.ruby.block.case",
				"xcode.lang.ruby.block.begin",
				"xcode.lang.ruby.block.do",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.block.until";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = until;
			End = end;
			Foldable = YES;
			Recursive = YES;
			StartAtBOL = YES;
			IncludeRules = (
				"xcode.lang.ruby.block",
				"xcode.lang.ruby.block.if",
				"xcode.lang.ruby.block.while",
				"xcode.lang.ruby.block.unless",
				"xcode.lang.ruby.block.for",
				"xcode.lang.ruby.block.case",
				"xcode.lang.ruby.block.begin",
				"xcode.lang.ruby.block.do",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.block.unless";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = unless;
			End = end;
			Foldable = YES;
			Recursive = YES;
			StartAtBOL = YES;
			IncludeRules = (
				"xcode.lang.ruby.block",
				"xcode.lang.ruby.block.if",
				"xcode.lang.ruby.block.while",
				"xcode.lang.ruby.block.until",
				"xcode.lang.ruby.block.for",
				"xcode.lang.ruby.block.case",
				"xcode.lang.ruby.block.begin",
				"xcode.lang.ruby.block.do",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.block.for";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = for;
			End = end;
			Foldable = YES;
			Recursive = YES;
			StartAtBOL = YES;
			IncludeRules = (
				"xcode.lang.ruby.block",
				"xcode.lang.ruby.block.if",
				"xcode.lang.ruby.block.while",
				"xcode.lang.ruby.block.until",
				"xcode.lang.ruby.block.unless",
				"xcode.lang.ruby.block.case",
				"xcode.lang.ruby.block.begin",
				"xcode.lang.ruby.block.do",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.block.case";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = case;
			End = end;
			Foldable = YES;
			Recursive = YES;
			StartAtBOL = YES;
			IncludeRules = (
				"xcode.lang.ruby.block",
				"xcode.lang.ruby.block.if",
				"xcode.lang.ruby.block.while",
				"xcode.lang.ruby.block.until",
				"xcode.lang.ruby.block.unless",
				"xcode.lang.ruby.block.for",
				"xcode.lang.ruby.block.begin",
				"xcode.lang.ruby.block.do",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.block.begin";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = begin;
			End = end;
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.ruby.block",
				"xcode.lang.ruby.block.if",
				"xcode.lang.ruby.block.while",
				"xcode.lang.ruby.block.until",
				"xcode.lang.ruby.block.unless",
				"xcode.lang.ruby.block.for",
				"xcode.lang.ruby.block.case",
				"xcode.lang.ruby.block.do",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.block.do";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = do;
			End = end;
			Foldable = YES;
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.ruby.block",
				"xcode.lang.ruby.block.if",
				"xcode.lang.ruby.block.while",
				"xcode.lang.ruby.block.until",
				"xcode.lang.ruby.block.unless",
				"xcode.lang.ruby.block.for",
				"xcode.lang.ruby.block.case",
				"xcode.lang.ruby.block.begin",
				"xcode.lang.ruby.bracketexpr",
				"xcode.lang.ruby.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.parenexpr";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = "(";
			End = ")";
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.ruby.bracketexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.bracketexpr";
		Syntax = {
			Tokenizer = "xcode.lang.ruby.lexer";
			Start = "[";
			End = "]";
			Recursive = YES;
			IncludeRules = (
				"xcode.lang.ruby.parenexpr",
			);
		};
	},
	{
		Identifier = "xcode.lang.ruby.stringeval";
		Syntax = {
			Start = "#{";
			End = "}";
		};
	},
	{
		Identifier = "xcode.lang.ruby.comment";
		Syntax = {
			Start = "=begin";
			End = "=end";
			Foldable = YES;
			IncludeRules = (
				"xcode.lang.url",
				"xcode.lang.url.mail",
				"xcode.lang.comment.mark",
			);
			Type = "xcode.syntax.comment";
		};
	},
)